package com.ppawluki.manager.json_model;

//Matcher Class file. Represents matcher object from .json.
public class Matcher {
    private String matcherRule;
    private String param;

    public String getMatcherRule() {
        return matcherRule;
    }

    public void setMatcherRule(String matcherRule) {
        this.matcherRule = matcherRule;
    }

    public String getParam() {
        return param;
    }

    public void setParam(String param) {
        this.param = param;
    }

    @Override
    public String toString() {
        return "Matcher{" +
                "matcherRule='" + matcherRule + '\'' +
                ", param='" + param + '\'' +
                '}';
    }
}
