package com.ppawluki.manager.json_model;

import java.util.List;
//Scripts class, represents scripts object from the .json file.
public class Scripts {
    private List<Script> scripts;

    public List<Script> getScripts() {
        return scripts;
    }

    @Override
    public String toString() {
        return "Scripts{" +
                "scripts=" + scripts +
                '}';
    }

    public void setScripts(List<Script> scripts) {
        this.scripts = scripts;
    }
}
