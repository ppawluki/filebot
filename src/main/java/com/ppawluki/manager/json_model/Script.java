package com.ppawluki.manager.json_model;

import java.util.List;
//Script class file. Represents single script object from the .json file
public class Script {
    private String name;
    private List<Matcher> matchers;
    private Action action;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Matcher> getMatchers() {
        return matchers;
    }

    public void setMatchers(List<Matcher> matchers) {
        this.matchers = matchers;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    @Override
    public String toString() {
        return "Skript{" +
                "name='" + name + '\'' +
                ", matchers=" + matchers +
                ", action=" + action +
                '}';
    }
}
