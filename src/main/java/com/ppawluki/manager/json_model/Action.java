package com.ppawluki.manager.json_model;

//Action Class file. Represents Action object from .json file
public class Action {
    private String actionName;
    private String actionParam;

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getActionParam() {
        return actionParam;
    }

    public void setActionParam(String actionParam) {
        this.actionParam = actionParam;
    }

    @Override
    public String toString() {
        return "Action{" +
                "actionName='" + actionName + '\'' +
                ", actionParam='" + actionParam + '\'' +
                '}';
    }
}
