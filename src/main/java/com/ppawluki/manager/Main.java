package com.ppawluki.manager;

import com.google.gson.Gson;
import com.ppawluki.manager.json_model.Script;
import com.ppawluki.manager.json_model.Scripts;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;

public class Main {

    public static final int ARGS_NUM= 4;

    public static void main(String[] args) {

        argsChecker(args);

        Gson gson = new Gson();
        try (Reader reader = new FileReader(args[3])) {
            Scripts scripts = gson.fromJson(reader, Scripts.class);
            for(Script script : scripts.getScripts()){
                ScriptHandler handler = new ScriptHandler(script, args[1]);
                handler.executeScript();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void argsChecker(String[] args){
        if(args.length != ARGS_NUM){
            throw new IllegalArgumentException("Not a valid number of arguments: "+ Arrays.asList(args).toString()
                    +"\nFound:["+args.length+"] instead of:["+ARGS_NUM+"]"
                    +"\nArguments order: --dir 'directory' --scripts 'script_name'");
        }
        if(!"--dir".equals(args[0])){
            throw new IllegalArgumentException("First argument should be --dir. Found this instead: "+ args[0]);
        }
        if(!"--scripts".equals(args[2])){
            throw new IllegalArgumentException("Third argument should be --scripts. Found this instead: "+ args[2]);
        }
        if(!args[3].matches(".+\\.json$")){
            throw new IllegalArgumentException("Fourth argument should have a .json extension. Found this instead:"+ args[3]);
        }


    }

}
