package com.ppawluki.manager;

import com.ppawluki.manager.json_model.Action;
import com.ppawluki.manager.json_model.Matcher;
import com.ppawluki.manager.json_model.Script;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class ScriptHandler {

    private Script script;
    private String directory;

    private List<Path> resultFiles;

    public ScriptHandler(Script script, String directory){
        this.script = script;
        this.directory = directory;
    }

    /*
    Script execution method. Loads matching files from given directory
    and executes given action on every matching file
     */
    public void executeScript(){
        resultFiles = getMatchingFiles(directory);
        if (resultFiles.isEmpty()){
            System.out.println("No matching files found!");
            return;
        }
        resultFiles.stream().forEach(file -> executeAction(file, script.getAction()));
    }

    /*
    Method gets files from the given directory and applies filters on them
     */
    private List<Path> getMatchingFiles(String directory){
        try{
            resultFiles = getFilesFromDir(directory);
            if(resultFiles.isEmpty()){
                return resultFiles;
            }
            resultFiles = filterFiles();
        }catch (FileNotFoundException e){
            System.out.println(e.getMessage());
        }
        return resultFiles;
    }

    /*
    Method used instead of "big lambda" inside the stream.
     */
    private void executeAction(Path file, Action action) {
        switch (action.getActionName().toUpperCase(Locale.ROOT)){
            case "COPYTO":{
                Path copy = Paths.get(action.getActionParam(), file.getFileName().toString());
                try {
                    Files.createDirectories(copy.getParent());
                    Files.copy(file, copy, StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException e) {
                    System.out.println("Couldn't copy indicated file: "+file.getFileName().toString());
                    e.printStackTrace();
                }
                break;
            }
            case "MOVETO":{
                Path copy = Paths.get(action.getActionParam(), file.getFileName().toString());
                try {
                    Files.createDirectories(copy.getParent());
                    Files.move(file, copy, StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException e) {
                    System.out.println("Couldn't move indicated file: "+file.getFileName().toString());
                    e.printStackTrace();
                }
                break;
            }
            case "DELETE": {
                try {
                    Files.delete(file);
                } catch (IOException e) {
                    System.out.println("Couldn't delete file: " + file.getFileName().toString());
                    e.printStackTrace();
                }
                break;
            }
            default:{
                System.out.println("\nAction not found!: "+action.getActionName());
            }
        }
    }

    private List<Path> filterFiles(){
        List<Path> filteredFiles = resultFiles;
        for (Matcher matcher : script.getMatchers()) {
            filteredFiles = filteredFiles.stream()
                    .filter(x-> applyFilter(x,matcher))
                    .collect(Collectors.toList());
        }
        return filteredFiles;
    }

    private List<Path> getFilesFromDir(String directory) throws FileNotFoundException {
        List<Path> result = null;
        try(Stream<Path> walk = Files.walk(Paths.get(directory))){
            result = walk.filter(Files::isRegularFile)
                    .collect(Collectors.toList());
            if(result.isEmpty())
                throw new FileNotFoundException("No files found under specified directory: "+directory);
            return result;
        }catch (IOException e){
            e.printStackTrace();
        }
        throw new FileNotFoundException("No files found under specified directory: "+directory);
    }

    private boolean applyFilter(Path file, Matcher matcher){
        switch(matcher.getMatcherRule().toUpperCase(Locale.ROOT)) {
            case "EXTENSIONIS": {
                return file.toString().endsWith(matcher.getParam());
            }
            case "EXTENSIONISNOT":{
                return !file.toString().endsWith(matcher.getParam());
            }
            case "NAMECONTAINS": {
                return file.toString().contains(matcher.getParam());
            }
            case "MODIFIEDDATELESSTHEN": {
                try {
                Date date = new SimpleDateFormat("yyyyMMdd").parse(matcher.getParam());
                FileTime timeMatcher = FileTime.fromMillis(date.getTime());
                    return Files.readAttributes(file, BasicFileAttributes.class)
                            .lastModifiedTime().compareTo(timeMatcher)<0;
                }catch(ParseException e){
                    System.out.println("Date format is not yyyyMMdd for: "+matcher.getParam());
                    e.printStackTrace();
                }catch (IOException e) {
                    System.out.println("Couldn't read files attributes for file: "+file.toString());
                    e.printStackTrace();
                }
                return false;
            }
            case "MODIFIEDDATEGREATERTHEN": {
                try {
                    Date date = new SimpleDateFormat("yyyyMMdd").parse(matcher.getParam());
                    FileTime timeMatcher = FileTime.fromMillis(date.getTime());
                    return Files.readAttributes(file, BasicFileAttributes.class)
                            .lastModifiedTime().compareTo(timeMatcher)>0;
                }catch(ParseException e){
                    System.out.println("Date format is not yyyyMMdd for: "+matcher.getParam());
                    e.printStackTrace();
                }catch (IOException e) {
                    System.out.println("Couldn't read files attributes for file: "+file.toString());
                    e.printStackTrace();
                }
            }
        }

        return false;
    }

}


